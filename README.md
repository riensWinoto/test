# Documentation

## Dockerfile
For dockerfile just add some addition to make it multi-stage and have unit test

## app-deployment
For app-deployment, just convert it to yaml and following from compose yaml and set replica to 2

## db-deployment
For db-deployment, following the compose yaml and add mountPath

## cache-deployment
For cache-deployment, just convert it to yaml and following from compose yaml and set replica to 2

## elk stack
Never using that, don't know what to do