FROM node:10.13-alpine AS base
WORKDIR /var/www/app
COPY . .

FROM base AS setup
RUN npm install --only=production

FROM setup AS test
RUN npm run test

FROM base AS release
COPY . .
EXPOSE 3000
CMD npm run start